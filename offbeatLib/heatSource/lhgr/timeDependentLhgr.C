/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "timeDependentLhgr.H"
#include "addToRunTimeSelectionTable.H"
#include "IndirectList.H"
    
// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(timeDependentLhgr, 0);
    
    addToRunTimeSelectionTable
    (
        heatSource, 
        timeDependentLhgr, 
        dictionary
    );
}

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


void Foam::timeDependentLhgr::calcAddressing
(
    const dictionary& dict
)
{
    addr_ = labelList();
    
    wordList zoneNames(dict.lookup("materials"));
    
    forAll(zoneNames, i)
    {
        const label zoneI = mesh_.cellZones().findZoneID(zoneNames[i]);
        
        if (zoneI == -1)
        {
            FatalIOErrorInFunction(dict.lookup("materials"))
                << "cellZone " << zoneNames[i] << " not found on mesh."
                << abort(FatalIOError);
        }
        
        addr_.append(mesh_.cellZones()[zoneI]);
    }
}


void Foam::timeDependentLhgr::calcReferenceDimensions
(
    const dictionary& dict
)
{
    // TODO - Since each point can be referenced by multiple cells, this algorithm
    // will likely count each point multiple times. A more efficient approach
    // is needed.
    const fvMesh& referenceMesh = 
    ( 
        mesh_.foundObject<fvMesh>("referenceMesh")
    ) ?
    mesh_.lookupObject<fvMesh>("referenceMesh")
    : 
    mesh_;

    const vectorField& points = referenceMesh.points();
    const labelListList& cellPoints = referenceMesh.cellPoints();
    scalarField z = (points & pinDirection_);
    scalarField r = mag(points - (z*pinDirection_));

    zMax_ = -GREAT;
    zMin_ = GREAT;
    rMax_ = 0;
    
    forAll(addr_, addrI)
    {
        const label cellI = addr_[addrI];
        const labelList& ptIds = cellPoints[cellI];
        
        forAll(ptIds, i)
        {
            const label ptI = ptIds[i];
            zMin_ = min(zMin_, z[ptI]);
            zMax_ = max(zMax_, z[ptI]);
            rMax_ = max(rMax_, r[ptI]);
        }
    }
    reduce(zMin_, minOp<scalar>());
    reduce(zMax_, maxOp<scalar>());
    reduce(rMax_, maxOp<scalar>());
    
    if(debug)
    {
        Info<< "Geometry calculated by timeDependentLhgr::calcReferenceDimensions" << nl
            << tab << "Pin direction: " << pinDirection_ << nl
            << tab << "Z bounds: " << zMin_ << "-" << zMax_ << nl
            << tab << "Radius: "   << rMax_ << nl << endl;
    }
}


// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::timeDependentLhgr::timeDependentLhgr
(
    const fvMesh& mesh,
    const materials& materials,
    const dictionary& heatSourceOptDict
)
:
    lhgr(mesh, materials, heatSourceOptDict),
    lhgrData_(heatSourceOptDict.lookup("lhgr")),
    timeData_(heatSourceOptDict.lookup("timePoints")),
    method_
    (
        interpolateTableBase::interpolationMethodNames_
        [
            heatSourceOptDict.lookupOrDefault<word>
            ("timeInterpolationMethod", "linear")
        ]
    ),
    lhgrTable_(timeData_, lhgrData_, method_),
    pinDirection_(),
    radialModel_(),
    axialModel_(),
    addr_()
{
    const globalOptions& globalOpt
    (mesh_.lookupObject<globalOptions>("globalOptions"));
    pinDirection_ = vector(globalOpt.pinDirection());

    calcAddressing(heatSourceOptDict);
    calcReferenceDimensions(heatSourceOptDict);
    
    radialModel_ = radialProfile::New
    (
        mesh, 
        heatSourceOptDict.subDict("radialProfile"), 
        addr_,
        pinDirection_,
        rMax_
    );

    axialModel_ = axialProfile::New
    (
        mesh, 
        heatSourceOptDict.subDict("axialProfile"), 
        addr_,
        pinDirection_,
        zMin_,
        zMax_
    );
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::timeDependentLhgr::~timeDependentLhgr()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void Foam::timeDependentLhgr::correct()
{
    const globalOptions& globalOpt
    (mesh_.lookupObject<globalOptions>("globalOptions"));

    scalar angularFraction = globalOpt.angularFraction();

    if(mesh_.topoChanging())
    {
        calcAddressing(heatSourceOptDict_);
        calcReferenceDimensions(heatSourceOptDict_);
    }

    if(!mesh_.foundObject<volScalarField>("QPrevIter"))
    {
        Q_.storePrevIter();
    }

    Q_.prevIter().storePrevIter();        
    Q_.storePrevIter();

    const scalarField& Qprev(Q_.prevIter());
    const scalarField& QprevPrev(Q_.prevIter().prevIter());

    //-Correct heat source only once per time step or until power changes
    if
    ( 
        gMax(mag(QprevPrev - Qprev)) > 1e-3 
        or
        mesh_.time().value() > currentTime_
    )
    {  
        currentTime_ = mesh_.time().value();

        scalarField Vi(mesh_.V(), addr_);
        scalar totalVol(gSum(Vi)/angularFraction);

        // Interpolate LHGR and calculate average power density Qavg
        scalar lhgr = lookupLHGR(currentTime_);
        scalar Qavg = lhgr*(zMax_ - zMin_)/totalVol;
        // Correct the radial and axial profiles
        radialModel_->correct();
        axialModel_->correct();

        // Build the resulting heat source
        scalarField& Qi = Q_.ref();
        scalarField& lhgri = lhgr_.ref();
        const scalarField& f_r = radialModel_->profile();
        const scalarField& g_z = axialModel_->profile();

        forAll(addr_, addrI)
        {
            const label cellI = addr_[addrI];
            
            Qi[cellI] = Qavg*f_r[addrI]*g_z[addrI];
            lhgri[cellI] = lhgr*g_z[addrI];
        }
        
        scalarField Qaddr(Q_.ref(), addr_);
        scalar powerTot   = lhgr*(zMax_ - zMin_)*angularFraction;
        scalar powerTotOF = gSum( Vi*Qaddr );
        scalar error = 
        100*
        (
            mag(powerTotOF - powerTot)/max(powerTot, VSMALL)
        );

        if(error > 0.01)
        {            
            WarningIn("Foam::timeDependentLhgr::correct()") << nl  
                << "    Total power does not correspond to LHGR*height, "<< nl 
                << "    with an error of "  << error << "%" << nl
                << "    Radial or axial profiles might be not normalized." << nl
                << "    The field Q will be scaled to preserve total power."
                <<  nl << endl;

            radialModel_ -> checkNormalization();
            axialModel_ -> checkNormalization();

            scalar scalingFactor(powerTot/powerTotOF);

            forAll(addr_, addrI)
            {
                const label cellI = addr_[addrI];
                
                Qi[cellI] *= scalingFactor;
            }
        }
    }
    else
    {
        currentTime_ = mesh_.time().value();
    }

    Q_.correctBoundaryConditions();
    lhgr_.correctBoundaryConditions();
}


Foam::scalar Foam::timeDependentLhgr::predictNextPowerDensity() const
{
    const globalOptions& globalOpt
    (mesh_.lookupObject<globalOptions>("globalOptions"));

    scalar angularFraction = globalOpt.angularFraction();
    
    //- Assume that deltaT does not change
    scalar predictedNextTime = min
    (
        currentTime_ + mesh_.time().deltaT().value(),
        mesh_.time().userTimeToTime
        (
            lhgrTable_.getNextPoint(mesh_.time().timeToUserTime(currentTime_))
        )
    );

    // Volumes of a subset of cells corresponding to addr_ indexing
    scalarField Vi(mesh_.V(), addr_);

    scalar totalVol(gSum(Vi)/angularFraction);

    // Interpolate LHGR and calculate average power density nextQavg
    scalar lhgr = lookupLHGR(predictedNextTime);
    scalar nextQavg = lhgr*(zMax_ - zMin_)/totalVol;

    // Build the resulting heat source
    const scalarField& f_r = radialModel_->profile();
    const scalarField& g_z = axialModel_->profile();
    scalarField nextQi = nextQavg * f_r * g_z;

    // if(mesh_.foundObject<fvMesh>("referenceMesh"))
    // {
    //     const fvMesh& refMesh =  mesh_.lookupObject<fvMesh>("referenceMesh");

    //     const scalarField& Vref =  refMesh.V();

    //     forAll(addr_, addrI)
    //     {
    //         const label cellI = addr_[addrI];

    //         nextQi[cellI] = nextQi[cellI]
    //                       * Vref[cellI] / Vi[cellI]; 
    //     }
    // }    

    scalar powerTot   = lhgr*(zMax_ - zMin_)*angularFraction;
    scalar powerTotOF = gSum( Vi*nextQi );
    scalar error = 
    100*
    (
        mag(powerTotOF - powerTot)/max(powerTot, VSMALL)
    );

    if(error > 0.01)
    {           
        scalar scalingFactor(powerTot/powerTotOF);
        
        nextQi *= scalingFactor;
    }

    return gSum(Vi*nextQi)/gSum(Vi);

}


Foam::scalar Foam::timeDependentLhgr::averagePowerDensity() const
{
    scalarField Vi(mesh_.V(), addr_);
    scalarField Qi(Q_, addr_);

    return gSum(Vi*Qi)/gSum(Vi);
}


Foam::scalar Foam::timeDependentLhgr::nextTimeMarker() const
{
    return min
    (
        min
        (
            mesh_.time().userTimeToTime
            (
                lhgrTable_.getNextPoint(mesh_.time().timeToUserTime(currentTime_))
            ),
            radialModel_->nextTimeMarker()
        ), 
        axialModel_->nextTimeMarker()
    );
}


Foam::scalar Foam::timeDependentLhgr::lastTimeMarker() const
{
    return min
    (
        min
        (
            mesh_.time().userTimeToTime(lhgrTable_.getLastPoint()), 
            radialModel_->lastTimeMarker()
        ), 
        axialModel_->lastTimeMarker()
    );
}

// ************************************************************************* //
