/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "swellingGrowthAIM11515Ti.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(swellingGrowthAIM11515Ti, 0);
    addToRunTimeSelectionTable(swellingModel, swellingGrowthAIM11515Ti, dictionary);
}

// * * * * * * * * * * * * * Static Member Functions * * * * * * * * * * * * //


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::swellingGrowthAIM11515Ti::swellingGrowthAIM11515Ti
(
    const fvMesh& mesh,
    const dictionary& dict
)
:
    swellingModel(mesh, dict),
    fastFluenceName_(dict.lookupOrDefault<word>("fastFluenceName", "fastFluence" )),
    fastFluence_(mesh_.lookupObject<volScalarField>(fastFluenceName_)),
    par1(1.3e-5),
    par2(490),
    par3(100),
    par4(3.9),
    perturb(1.0)
{    
    if(dict.found("swelling"))
    {
        const dictionary& swellingDict = dict.subDict("swelling");

        par1 = swellingDict.lookupOrDefault<scalar>("par1", 1.3e-5);
        par2 = swellingDict.lookupOrDefault<scalar>("par2", 490);
        par3 = swellingDict.lookupOrDefault<scalar>("par3", 100);
        par4 = swellingDict.lookupOrDefault<scalar>("par4", 3.9);

        perturb = swellingDict.lookupOrDefault<scalar>("perturb", 1.0);
    }  
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::swellingGrowthAIM11515Ti::~swellingGrowthAIM11515Ti()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void Foam::swellingGrowthAIM11515Ti::correct
(
    const scalarField& T, 
    const labelList& addr
)
{
    symmTensorField& swellingI = epsilonSwelling_.ref();

    forAll(addr, addrI)
    {
        const label cellI = addr[addrI];
        
        const scalar Ti = T[cellI] - 237.15;

        //- Divide fast fluence by 1e22 n cm^-2
        const scalar fastFluencei = fastFluence_[cellI]*1e-22;
        
        const scalar nominalValue = 
        par1 * exp(-(pow((Ti-par2)/par3,2)))*pow(fastFluencei,par4);

        swellingI[cellI] = nominalValue*perturb;
    }

    // Check which F and delta should be used
    const dimensionedScalar& F = 
    isInUserParameters(this->group(), "F_epsilonSwelling") ?
    this->F_epsilonSwelling() : 
    swellingModel::F_epsilonSwelling();

    const dimensionedScalar& delta = 
    isInUserParameters(this->group(), "delta_epsilonSwelling") ?
    this->delta_epsilonSwelling() : 
    swellingModel::delta_epsilonSwelling();

    // Perturb epsilon swelling for sensitivity analysis
    applyParameters(epsilonSwelling_.ref(), addr, F, delta);

}


// ************************************************************************* //
