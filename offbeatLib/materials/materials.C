/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "materials.H"
#include "zeroGradientFvPatchField.H"
#include "debug.H"
#include "userParameters.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(materials, 0);
    defineRunTimeSelectionTable(materials, dictionary);
}

// * * * * * * * * * * * * * Static Member Functions * * * * * * * * * * * * //


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::materials::materials
(
    const fvMesh& mesh, 
    const dictionary& materialsDict
)
:
    mesh_(mesh),
    materialsDict_(materialsDict),
    T_(nullptr),
    rho_
    (
        IOobject
        (
            "rho",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh_,
        dimensionedScalar("rho", dimMass/dimVolume, 0),
        zeroGradientFvPatchField<scalar>::typeName
    ),
    Cp_
    (
        IOobject
        (
            "Cp",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh_,
        dimensionedScalar("Cp", dimSpecificHeatCapacity, 0),
        zeroGradientFvPatchField<scalar>::typeName
    ),
    k_
    (
        IOobject
        (
            "k",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh_,
        dimensionedScalar("k", dimPower/dimLength/dimTemperature, 0),
        zeroGradientFvPatchField<scalar>::typeName
    ),
    emissivity_
    (
        IOobject
        (
            "emissivity",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh_,
        dimensionedScalar("emissivity", dimless, 0),
        zeroGradientFvPatchField<scalar>::typeName
    ),
    E_
    (
        IOobject
        (
            "E",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh_,
        dimensionedScalar("E", dimPressure, 0),
        zeroGradientFvPatchField<scalar>::typeName
    ),
    nu_
    (
        IOobject
        (
            "nu",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh_,
        dimensionedScalar("nu", dimless, 0),
        zeroGradientFvPatchField<scalar>::typeName
    ),
    mu_
    (
        IOobject
        (
            "mu",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh_,
        dimensionedScalar("mu", dimPressure, 0),
        zeroGradientFvPatchField<scalar>::typeName
    ),
    lambda_
    (
        IOobject
        (
            "lambda",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh_,
        dimensionedScalar("lambda", dimPressure, 0),
        zeroGradientFvPatchField<scalar>::typeName
    ),
    threeK_
    (
        IOobject
        (
            "threeK",
            mesh_.time().timeName(),
            mesh_,
            IOobject::NO_READ,
            IOobject::NO_WRITE
        ),
        mesh_,
        dimensionedScalar("threeK", dimPressure, 0),
        zeroGradientFvPatchField<scalar>::typeName
    )
{}


Foam::autoPtr<Foam::materials>
Foam::materials::New
(
    const fvMesh& mesh, 
    const dictionary& solverDict
)
{
    // Initialize type for materials class
    word type;

    dictionary materialsDict(solverDict.subOrEmptyDict("materials"));
    solverDict.lookup("materialProperties") >> type;

    Info << "Selecting materials model " << type << endl;

    dictionaryConstructorTable::iterator cstrIter
        = dictionaryConstructorTablePtr_->find(type);

    if (cstrIter == dictionaryConstructorTablePtr_->end())
    {
        FatalErrorIn("materials::New(const fvMesh&, const dictionary&)")
            << "Unknown materials type "
            << type << nl << nl
            << "Valid types are:" << endl
            << dictionaryConstructorTablePtr_->toc()
            << exit(FatalError);
    }

    if (debug)
    {
        Info<< "Selecting materials type "
            << type << endl;
    }

    return autoPtr<materials>(cstrIter()(mesh, materialsDict));
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::materials::~materials()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void Foam::materials::updateDensity()
{
    if(mesh_.foundObject<fvMesh>("referenceMesh"))
    {
        //- Constant reference to reference volumes
        const fvMesh& refMesh =  mesh_.lookupObject<fvMesh>("referenceMesh");
        const scalarField& Vref =  refMesh.V();

        forAll(mesh_.cellZones(), zoneI)
        {
            const labelList& addr = mesh_.cellZones()[zoneI];

            forAll(addr, i)
            {
                const label cellI = addr[i];

                //- Rescale the cell density with the ref/new volume ratio 
                rho_[cellI] = rho_[cellI] 
                            * Vref[cellI] 
                            / mesh_.V()[cellI];
            }
        }

        rho_.correctBoundaryConditions();
    }
}


// ************************************************************************* //
