import numpy as np
import subprocess
import os

dirlist = [ item for item in os.listdir('./') if os.path.isdir(os.path.join('./', item)) ]
dirlist

enrichments = dirlist

for j in range(0,len(enrichments)):

	fname=str(enrichments[j])+'/UO2pellet_mdx1.m'

	# Extract XS_0 vector from the result file and store in .tmp file
	command = 'cat '+fname+' | grep -A 1000000 "XS_0" > '+fname+'.tmp'
	subprocess.check_output(command, shell=True)

	# Store XS_0 data
	XS_0 = np.genfromtxt(fname+'.tmp', skip_header=1, skip_footer=1)

	# Delete .tmp file
	os.remove(fname+'.tmp')

	# Initialize fission, capture, elastic, inelastic scatt.
	xs_f = []
	xs_c = []
	xs_es = []
	xs_is = []

	# Initialize ID nuclides
	idNuclides = [ 	'iU235',
					'iU238',
					'iPu239',
					'iPu240',
					'iPu241',
					'iPu242'
				  ]

	for i in range(0,len(XS_0[:,0])):
		# Fission xs
		if(XS_0[i,1] == 18):
			xs_f.append(XS_0[i,5])

		# Capture xs
		if(XS_0[i,1] == 101):
			xs_c.append(XS_0[i,5])

		# Elastic sc xs
		if(XS_0[i,1] == 2):
			xs_es.append(XS_0[i,5])

		# Inelastic sc xs
		if(XS_0[i,1] == 4):
			xs_is.append(XS_0[i,5])

	# Compute thermal absorption xs
	xs_a_th = []
	for i in range(0,len(xs_f)):
		xs_a_th.append('{:.2f}'.format(xs_f[i] + xs_c[i]))

	# Compute total xs
	xs_t = []
	for i in range(0,len(xs_f)):
		xs_t.append('{:.2f}'.format(xs_es[i] + xs_is[i] + xs_f[i] + xs_c[i]))

	f = open('enrichment_'+str(enrichments[j]), 'w')
	f.write('xs data from file : '+fname+'\n')

	f.write('// Thermal absorption :\n')
	for i in range(0,len(xs_a_th)):
		f.write('sa_th_['+idNuclides[i]+'] = '+str(xs_a_th[i])+'e-24;\n')

	f.write('// Total :\n')
	for i in range(0,len(xs_t)):
		f.write('st_th_['+idNuclides[i]+'] = '+str(xs_t[i])+'e-24;\n')

	f.write('\n')
	f.close()

