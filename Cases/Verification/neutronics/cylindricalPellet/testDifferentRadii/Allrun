#!/bin/bash
cd ${0%/*} || exit 1    # run from this directory

# Useful paths
sDict=./constant/solverDict
pDict=./system/radialProfile

rm -fr results
mkdir results

radii=$(seq 1 1 10)

for radius in $radii
do
	# Copy base folder
	cp -fr baseCase baseCase_$radius
	cd baseCase_$radius

	# Change fuel outer radius
	cd rodMaker
	sed -i "/rOuterFuel/c\'rOuterFuel': [$radius]," rodDict 
	python3 rodMaker.py 
	sed -i 's/regionCoupledOFFBEAT/patch/' 	blockMeshDict 
	sed -i '/fuelInnerSurface/,/}/d' 		blockMeshDict 
	mv blockMeshDict ../system
	cd ..

	# Create mesh
	blockMesh 			> log.blockMesh
	changeDictionary 	> log.changeDictionary

	# Set the solverDict for diffusion
	foamDictionary $sDict -entry neutronics -set diffusion > log.foamDictionary
	foamDictionary $sDict -entry burnup -set Lassmann >> log.foamDictionary

	# Run the case
	foamListTimes -rm 	> log.foamListTimes
	offbeat 			> log.offbeat

	# Store probed file
	lastTimeFolder=$(foamListTimes | tail -n 1)
	cp postProcessing/radialProfile/$lastTimeFolder/a_formFactor.xy ../results/diffusion_$radius
	cp ../dataTUBRNP/TUBRNP_$radius ../results

	# Change directory and remove folder
	cd ..

done

cd results

for radius in $radii; do
    gnuplot <<- EOF
        set xlabel "radius (m)"
        set ylabel "formFactor (-)"
        set output "${radius}.png" 
        set term png 
        plot "TUBRNP_${radius}" using 1:2 title 'TUBRNP' lw 4 lt 6, \
        	 "diffusion_${radius}" using 1:2 title 'diffusion' lw 2 lt -1 with lines
EOF
done

for radius in $radii; do
    python3 <<- EOF
import numpy as np
T=np.genfromtxt("TUBRNP_${radius}")
D=np.genfromtxt("diffusion_${radius}")
relDiff = (D[:,1]-T[:,1])/T[:,1]*100
with open('maxDiff','w') as f:
    f.write("{:.4f}\n".format(max(relDiff)))
EOF
done

rm -f diffusion_*
rm -f TUBRNP_*


