set terminal png
set output 'sigma.png'
set title "Stress Components"
set xlabel "x (cm)"
set ylabel "Stress (MPa)"
plot "postProcessing/xyGraph/1/radialProfile_sigma_epsilon.xy" using (100*$1):($2/1e6) title "x" with lines, \
     "postProcessing/xyGraph/1/radialProfile_sigma_epsilon.xy" using (100*$1):($5/1e6) title "y" with lines, \
     "postProcessing/xyGraph/1/radialProfile_sigma_epsilon.xy" using (100*$1):($3/1e6) title "xy" with lines

set output 'epsilon.png'
set title "Strain Components"
set xlabel "x (cm)"
set ylabel "Strain (-)"
plot "postProcessing/xyGraph/1/radialProfile_sigma_epsilon.xy" using (100*$1):8 title "x" with lines, \
     "postProcessing/xyGraph/1/radialProfile_sigma_epsilon.xy" using (100*$1):11 title "y" with lines, \
     "postProcessing/xyGraph/1/radialProfile_sigma_epsilon.xy" using (100*$1):9 title "xy" with lines
