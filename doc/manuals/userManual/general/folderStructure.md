# Case Folder Structure {#folderStructure}

This guide gives an overview on the standard structure of an OFFBEAT case directory.

* `0/` folder (or the last time step folder if `latestTime` is selected as `startFrom` in `controlDict`). It contains the initial and boundary conditions for the main fields, typically:
   * `T`, Initial and boundary conditions for the temperature field.
   * `D`, Initial and boundary conditions for the total displacement field.
   * `DD`, If an incremental mechanical subsolver is used, it replaces the file D
   * `gapGas`, information on the initial gapGas composition and pressure.

* `constant/` folder. It contains:
   * The `polyMesh/` folder, with mesh connectivity (e.g. points, faces, etc)
   * The `solverDict` dictionary for the solvers selection and model activation. It is the main OFFBEAT dictionary. 
   
* `system/` folder. As a minimum, it contains the following files:
   * `controlDict`, provides the main parameters controlling the simulation. Start time, end time, adaptive time step options, etc. are all set within this dictionary. 
   * `fvSchemes`, indicates the settings for the discretization schemes used by the code, i.e. time derivatives, gradients, laplacians, etc.  
   * `fvSolution`, indicates the settings for the numerical solvers used by the code.

The `system/` folder may additionally contain any number of other files that are used by the other OpenFOAM tools during model pre- and post-processing:
   * `blockMeshDict`, provides instructions for creating the mesh using OpenFOAM's `blockMesh` utility. The mesh can also be created with any number of external tools like [Salome Plaform](https://www.salome-platform.org/), [Gmsh](https://gmsh.info/), bypassing `blockMesh`. OpenFOAM provides a number of mesh conversion utilities for this purpose, see the [OpenFOAM User Guide](https://cfd.direct/openfoam/user-guide/v9-mesh) for more details.
   * `changeDictionaryDict`, provides instruction to modify the patch boundaries or other dictionaries. Read by the 'changeDictionary' utility.
   * `probes`, indicates the position (in the mesh) of the probes used to record quantities during runtime, see the [OpenFOAM User Guide](https://cfd.direct/openfoam/user-guide/v9-graphs-monitoring) for more details.
 
An OFFBEAT folder might contain the additional files (not necessary): 
* `Allrun` and `Allclean` bash scripts or similar, used to sequentially perform a set of commands to respectively run and clean the case. 
* `Residuals.gp` or similar gnuplot script, used to visualize the residuals of the at runtime.
* `plot.py` or similar pyhon scripts, a simple tool to plot quantities typically recored in the folder postProcessing/ (saved by the probes or by the sample utility).

***

Return to [Case Folder Structure and Workflow ](@ref general)
