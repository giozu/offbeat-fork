# Setting the solverDict file {#solverDict}

The entire simulation, including the choice of governing equations, physical models, material properties, power history, etc., is defined by the `solverDict` located in the `constant` subdirectory of the case.

The first entries required by `solverDict` define the type of problem and underlying physics to solve for.

| Keyword   	                               | Description                                                        	| Type    	|
|--------------------------------------------  |--------------------------------------------------------------------	|---------	|
| [thermalSolver](@ref thermal)  	           | Select the solution type for the heat transfer 	                    | word 	|
| [mechanicsSolver](@ref mechanics)            | Select the solution type for the momentum balance  	                | word 	|
| [neutronicsSolver](@ref neutronics)          | Select the solution type for the neutron flux distribution             | word 	|
| [gapGas](@ref gapGas)                        | Select the gapGas model                             	                | word 	|
| [heatSourceModel](@ref heatSource)           | Select the model type for the heat source          	                | word 	|
| [fastFluxModel](@ref fastFlux) 	           | Select the model type for the fast flux and fast fluence   	        | word 	|
| [burnup](@ref burnup)  	                   | Select the model type for the burnup            	                    | word 	|
| [fgr](@ref fgr) 	    	                   | Select the model type for the fission gas release  	                | word 	|
| [materialProperties](@ref materialProperties)| Select the material property treatement                                | word 	|
| [rheology](@ref rheology)   	               | Select the rheology law treatement                                     | word 	|
| [mapperModel](@ref mapper)   	               | Select the type of 3D to 1D mapping                                    | word 	|

For most underlying physics or models, an additional subdictionary might be required to further define the physics and solution methods.

***

Return to [User Manual](@ref userManual)
