# Fission Gas Release Model {#fgr}
The fission gas release (FGR) model type is selected with the `fgr` keyword in the *solverDict* dictionary. Currently OFFBEAT supports the following FGR models:
- By selecting the mother class fissionGasRelease.H with the keyword `none`, the fission gas behavior is neglected and the gap composition does not change during irradiation.
- [SCIANTIX](https://gitlab.com/poliminrg/sciantix), the 0-D open-source code SCIANTIX (developed at the PoliMi) is used as the local FGR module. SCIANTIX is included in OFFBEAT by means of the wrapper class fgrSCIANTIX.H . 

### SCIANTIX
When selecting the `SCIANTIX` FGR model, OFFBEAT passes the local variables to SCIANTIX which calculate the new values for the current cell. SCIANTIX considers intra- and (mechanistic) inter-granular behavior, burst release, grain growth and gaseous swelling.

To avoid the creation of several volumetric fields, the SCIANTIX variables (one value per cell) are created as simple fields (i.e. list). By default the fields are read from a regIOObject called 'SCIANTIX_fields' that is printed at each (writing) time step. This dictionary contains also a summary of the current fgr%. If not present in this dictionary, the fields are created with a default initial value.

However, if an IOObject (e.g. a volumetric field such a volScalarField) corresponding to one of the SCIANTIX variables is present in the initial time step folder, the field is read from this file and NOT from the 'SCIANTIX_fields' dicionary. In subsequent time step, the field is written once again in the 'SCIANTIX_fields' dicionary. This might be useful when performing advanced operations such as using mapping routines available in OpenFOAM.

In some cases, it might be necessary to add the SCIANTIX variables as volumetric fields in registry (e.g. for plotting them in paraFoam, for probing them or mapping them). One can use the keyword "addToRegistry" in the fgr option dictionary. In this case, a volumetric field per SCIANTIX variable is created, added to the registry and written to disc at every writing time step.

SCIANTIX requires the file *input_settings.txt* in the case folder. An example of the input_settings file is given in the header of fgrSCIANTIX.H (see SCIANTIX documentation for more details).


***

Return to [Setting the `solverDict`](@ref solverDict)
