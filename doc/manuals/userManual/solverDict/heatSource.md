# Heat source model {#heatSource}

The heat source model type is selected with the `heatSource` keyword in the *solverDict* dictionary. The units of the heat source are (W/m3).

Currently OFFBEAT supports the following heat source models:
- By selecting the mother class heatSource.H with the keyword `fromLatestTime`, i.e. the heat source field remains as defined in the initial time folder (by default equal to 0 W/m3 if the `Q` file is missing).
- lhgr.H, same as `fromLatestTime` but it additionally generates the linear heat rate field `lhgr` .
- timeDependentLhgr.H, time dependent average linear heat generation rate (lhgr). It offers the possibility of having a radial and axial profile.

***

Return to [Setting the `solverDict`](@ref solverDict)
