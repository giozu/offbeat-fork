# Neutronics Solution {#neutronics}

OFFBEAT is provided with a [neutronics sub-solver](neutronicsSubSolver.H) to solve for the neutron flux distribution within the fuel pellets.

In its current implementation, OFFBEAT is provided with a single neutron diffusion solver ( see diffusionSolver.H ), which solves the following equation:
\f[
		D\nabla^2 \Phi = \Sigma_A\Phi
\f]
where:
- \f$ D \f$ is the neutron diffusion coefficient
- \f$ \Phi \f$ is the neutron thermal flux 
- \f$ \Sigma_A \f$ is the macroscopic absorption cross section

The obtained thermal neutron flux shape is used by the class burnupLassmann.H to compute the intra-pin power distribution. The solver considers the fuel pin to be a strongly absorbing medium for thermal neutrons, and therefore only diffusion and absortion terms appear in the equation. The boundary condition for neutron thermal flux needed by the solver can be selected by the user to a value of choice. Since only the thermal neutron shape is of importance for burnupLassmann.H, the user could simply select the boundary condition for flux to be a `fixedValue` to 1. 


***

Return to [Setting the `solverDict`](@ref solverDict)