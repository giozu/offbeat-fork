# Displacement Boundary Conditions {#displacementBCs}

This section summarizes the main displacement boundary conditions currently available in OFFBEAT. All the available options are quickly visible using the "banana method", i.e. using a dummy keywords (e.g "banana") for the boundary condition type. Running the case OpenFOAM will detect the mistake and propose a set of possible valid alternatives to the dummy keyword.

**NOTE** that the displacement is a vector field, so the user should be aware that certain keywords in the BC definition require vector values.

### Fixed Displacement boundary conditions
BCs that impose a displacement value (or time list) on a patch.

| Boundary Condition Name      | Brief Description                                                                                      | Class                                            |
|------------------------------|--------------------------------------------------------------------------------------------------------|--------------------------------------------------|
| `fixedDisplacement`          | Imposes a fixed displacement on the patch, which can be provided as a fixed value or as a time series. | fixedTemperatureFvPatchScalarField.H             | 
| `fixedDisplacementZeroShear` | Fixes the boundary normal component of the displacement and the shear stress to zero.                  | fixedDisplacementZeroShearFvPatchVectorField.H   |


### Imposed pressure boundary conditions


| Boundary Condition Name      | Brief Description                                                                                      | Class                                                 |
|------------------------------|--------------------------------------------------------------------------------------------------------|-------------------------------------------------------|
| `coolantPressure`            | A normal pressure is applied equal to fluid pressure with zero shear stress.                           | coolantPressureFixedDisplacementFvPatchVectorField.H  | 
| `tractionDisplacement`       | It models a fixed traction + normal pressure boundary condition.                                       | tractionDisplacementFvPatchVectorField.H              |
| `plenumSpringPressure`       | BC designed for the top fuel and top cap inner patches, it accounts for plenum pressure and spring     | plenumSpringPressureFvPatchVectorField.H              |
| `topCladRingPressure`        | BC designed for the top cladding ring (when top cap is not modeled)                                    | topCladRingPressureFvPatchVectorField.H               |
| `gapPressure`                | It applies the gap pressure on the patch (NOTE: it requires a gapGas model)                            | gapPressureFvPatchVectorField.H                       |


### Coupled boundary conditions

| Boundary Condition Name      | Brief Description                                                                                      | Class                                                 |
|------------------------------|--------------------------------------------------------------------------------------------------------|-------------------------------------------------------|
| `gapContact`                 | BC designed for fuel-clad gap. Applies gas pressure if the gap is not closed and contact p otherwise.  | gapContactFvPatchVectorField.H                        | 
| `implicitContact`            |                                                                                                        |                                                       | 
| `frictionalContact`          |                                                                                                        |                                                       | 

<img src="https://upload.wikimedia.org/wikipedia/commons/1/1a/C%C3%B4ne_orange_-_under_construction.png"  width="100">


***

Return to [Fields and Boundary Conditions](@ref fieldsAndBCs)


