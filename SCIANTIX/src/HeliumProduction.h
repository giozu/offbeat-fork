///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: L. Cognini                  //
//           D. Pizzocri and T. Barani   //
///////////////////////////////////////////

#include "GlobalVariables.h"
#include "FissionYield.h"
#include "Solver.h"

void HeliumProduction( );
