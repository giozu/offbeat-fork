///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2020                        //
//  Authors: A.Scolaro                   //
//                                       //
///////////////////////////////////////////

#include "GlobalVariables.h"

void SetOptions(int Sciantix_options[], double Sciantix_scaling_factors[]);
